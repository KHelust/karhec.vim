" KarHec Vim-Airline Theme
" Code licensed under the AGPL2+
"
" @author Bioka <dunkelheit.bioka@gmail.com>
" @author KHelust <hectorestrada72@gmail.com>

" Color palette {{{1
let s:gui01 = "#424242"
let s:gui02 = "#757575"
let s:gui03 = "#ffd740"
let s:gui04 = "#40c4ff"
let s:gui05 = "#ff5252"
let s:gui06 = "#18ffff"
let s:gui07 = "#69f0ae"
let s:gui08 = "#f8bbd0"
let s:ctm01 = "237"
let s:ctm02 = "109"
let s:ctm03 = "220"
let s:ctm04 = "39"
let s:ctm05 = "203"
let s:ctm06 = "80"
let s:ctm07 = "114"
let s:ctm08 = "211"

let s:guiWh = "#f5f5f5"
let s:guiBl = "#212121"
let s:ctmWh = "254"
let s:ctmBl = "234"

let s:guiCC = "#bdbdbd"
let s:ctmCC = "253"

" Normal mode {{{1
let s:N1 = [ s:guiBl , s:gui04 , s:ctmBl , s:ctm04 ]
let s:N2 = [ s:guiBl , s:gui02 , s:ctmBl , s:ctm02 ]
let s:N3 = [ s:guiWh , s:gui01 , s:ctmWh , s:ctm01 ]

" Insert mode {{{1
let s:I1 = [ s:guiBl , s:gui07 , s:ctmBl , s:ctm07 ]
let s:I2 = [ s:guiBl , s:gui02 , s:ctmBl , s:ctm02 ]
let s:I3 = [ s:guiWh , s:gui01 , s:ctmWh , s:ctm01 ]

" Visual mode {{{1
let s:V1 = [ s:guiBl , s:gui03 , s:ctmBl , s:ctm03 ]
let s:V2 = [ s:guiBl , s:gui02 , s:ctmBl , s:ctm02 ]
let s:V3 = [ s:guiWh , s:gui01 , s:ctmWh , s:ctm01 ]

" Replace mode {{{1
let s:R1 = [ s:guiBl , s:gui05 , s:ctmBl , s:ctm05 ]
let s:R2 = [ s:guiBl , s:gui02 , s:ctmBl , s:ctm02 ]
let s:R3 = [ s:guiWh , s:gui01 , s:ctmWh , s:ctm01 ]

" File changed {{{1
let s:changed = [ s:guiBl , s:guiCC , s:ctmBl , s:ctmCC ]

let g:airline#themes#karhec#palette = {}
let g:airline#themes#karhec#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)
let g:airline#themes#karhec#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)
let g:airline#themes#karhec#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)
let g:airline#themes#karhec#palette.replace = airline#themes#generate_color_map(s:R1, s:R2, s:R3)

" Inactive mode {{{1
let s:IN1 = [ s:gui02 , s:guiWh , s:ctm02 , s:ctmWh ]
let s:IN2 = [ s:gui03 , s:gui01 , s:ctm03  , s:ctm01 ]
let s:IA = [ s:IN1[1] , s:IN2[1] , s:IN1[3] , s:IN2[3] , '' ]
let g:airline#themes#karhec#palette.inactive = airline#themes#generate_color_map(s:IA, s:IA, s:IA)

" Warning info {{{1
let s:WARNING = [ s:guiBl, s:gui03, s:ctmBl, s:ctm03 ]
let s:ERROR = [ s:guiWh, s:gui05, s:ctmWh, s:ctm05 ]

let g:airline#themes#karhec#palette.normal.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.insert.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.visual.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.replace.airline_warning = s:WARNING

let g:airline#themes#karhec#palette.normal.airline_error = s:ERROR
let g:airline#themes#karhec#palette.insert.airline_error = s:ERROR
let g:airline#themes#karhec#palette.visual.airline_error = s:ERROR
let g:airline#themes#karhec#palette.replace.airline_error = s:ERROR

" File modified and not saved {{{1
let g:airline#themes#karhec#palette.normal_modified = airline#themes#generate_color_map(s:N1, s:N2, s:changed)
let g:airline#themes#karhec#palette.insert_modified = airline#themes#generate_color_map(s:I1, s:I2, s:changed)
let g:airline#themes#karhec#palette.replace_modified = airline#themes#generate_color_map(s:R1, s:R2, s:changed)
let g:airline#themes#karhec#palette.visual_modified = airline#themes#generate_color_map(s:V1, s:V2, s:changed)

let g:airline#themes#karhec#palette.normal_modified.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.insert_modified.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.visual_modified.airline_warning = s:WARNING
let g:airline#themes#karhec#palette.replace_modified.airline_warning = s:WARNING

let g:airline#themes#karhec#palette.normal_modified.airline_error = s:ERROR
let g:airline#themes#karhec#palette.insert_modified.airline_error = s:ERROR
let g:airline#themes#karhec#palette.visual_modified.airline_error = s:ERROR
let g:airline#themes#karhec#palette.replace_modified.airline_error = s:ERROR

" CtrlP {{{1
if !get(g:, 'loaded_ctrlp', 0)
  finish
endif

let s:CP1 = [ s:guiWh , s:gui01 , s:ctmWh , s:ctm01 ]
let s:CP2 = [ s:guiBl , s:gui02 , s:ctmBl , s:ctm02 ]
let s:CP3 = [ s:guiWh , s:gui08 , s:ctmWh , s:ctm08 ]

let g:airline#themes#karhec#palette.ctrlp = airline#extensions#ctrlp#generate_color_map(s:CP1, s:CP2, s:CP3)
" }}}

" FoldMethod {{{1
" vim:foldmethod=marker

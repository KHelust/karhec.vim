# karhec.vim

A dark Vim/Neovim colorscheme & AirlineTheme for GUI/TUI true color based on [dracula](https://github.com/dracula/vim), [onedark](https://github.com/joshdick/onedark.vim), & [material](https://github.com/kaicataldo/material.vim), with colors taking from [MaterialPalette](https://www.materialpalette.com/colors). 

![karhec.vim](./karhec.png)

# Installation

Clone the repository an then copy or link the content in your Vim/Neovim directory.

> `git clone https://gitlab.com/KHelust/karhec.vim`

> `cd karhec.vim`

> `cp -r autoload/ colors/ .vim/`

If there is an autoload directory then do next:

> `cd autoload`

> `cp -r airline/ .vim/autoload`

Using [Vim-Plug](https://github.com/junegunn/vim-plug) isn't avalaible.

# Configuration

## ColorScheme

Add the following to your `~/.vimrc` or `./vim/vimrc` if you use Vim, instead adding to your `.config/nvim/init.vim`.

> `syntax on`

> `colorscheme karhec`

## AirlineThme

To use the included `vim-airline` theme:

> `let g:airline_theme = 'karhec'`

# Preview Image

Preview image was taken using:

+ Terminal Emulator: [Konsole](https://kde.org/applications/en/system/org.kde.konsole)
+ OS: [Archlinux](https://www.archlinux.org/)
+ Desktop Environment: [Plasma 5](https://kde.org/plasma-desktop)
+ Window Manager: [i3](https://i3wm.org/)

# License

[MIT License](https://opensource.org/licenses/MIT)

" KarHec Theme v1.0.0
"
" @author Bioka <dunkelheit.bioka@gmail.com>
" @author KHelust <hectorestrada72@gmail.com>

" Config {{{1
set background=dark
highlight clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "karhec"
" }}}

" GUI Colors {{{1
" General {{{2
" Modes {{{3
hi Normal 		 				        guifg=#f5f5f5	  guibg=NONE      gui=NONE
hi Visual 		 				        guifg=#212121	  guibg=#ffd740 	gui=NONE
" Completion {{{3
hi Pmenu 		 				          guifg=#757575   guibg=#212121	  gui=NONE
hi PmenuSel 	 				        guifg=#212121   guibg=#c8e6c9 	gui=NONE
" Cursor {{{3
hi ColorColumn 	 				      guifg=NONE 		  guibg=#424242 	gui=NONE
hi Cursor 		 				        guifg=#f5f5f5	  guibg=NONE   	  gui=NONE
hi CursorColumn 				      guifg=NONE      guibg=#424242 	gui=NONE
hi CursorLine 	 				      guifg=NONE 			guibg=#424242 	gui=NONE
hi CursorLineNr	 				      guifg=#ffd740 	guibg=#424242 	gui=NONE
hi LineNr 		 				        guifg=#40c4ff 	guibg=NONE    	gui=NONE
" Diff  {{{3
hi DiffAdd 		 				        guifg=#212121 	guibg=#f8bbd0 	gui=bold
hi DiffChange 	 				      guifg=NONE    	guibg=#424242 	gui=NONE
hi DiffDelete 	 				      guifg=#212121 	guibg=#ff5252   gui=NONE
hi DiffText 	 				        guifg=#212121 	guibg=#ffd740 	gui=bold
" Fold {{{3
hi FoldColmun 	 				      guifg=#7986cb 	guibg=NONE    	gui=NONE
hi Folded 		 				        guifg=#7986cb 	guibg=NONE    	gui=NONE
" Messages {{{3
hi ErrorMsg 	 				        guifg=#f5f5f5 	guibg=#ff5252 	gui=NONE
hi WarningMsg 	 				      guifg=#f5f5f5 	guibg=#ffd740 	gui=NONE
" Search {{{3
hi IncSearch 	 				        guifg=#181818 	guibg=#ffecb3 	gui=none
hi Search 		 				        guifg=#181818 	guibg=#c8e6c9 	gui=none
" Split {{{3
hi StatusLine 		 			      guifg=#f5f5f5 	guibg=#424242 	gui=bold
hi StatusLineNC  				      guifg=#f5f5f5 	guibg=#424242 	gui=NONE
hi VertSplit 	 				        guifg=#424242 	guibg=#424242 	gui=bold
" Constant {{{3
hi Boolean 		 				        guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi Character 	 				        guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi Constant 	 				        guifg=NONE 		  guibg=NONE 		  gui=NONE
hi Float 		 				          guifg=#18ffff 	guibg=NONE  	  gui=NONE
hi Number 		 				        guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi String 		 				        guifg=#ffecb3 	guibg=NONE 		  gui=NONE
hi Underlined 	 				      guifg=NONE 		  guibg=NONE 		  gui=underline
" Indentifier {{{3
hi Function 	 				        guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi Identifier 	 				      guifg=#ff80ab 	guibg=NONE 		  gui=italic
" Statement {{{3
hi Conditional 	 				      guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi Keyword 		 				        guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi Label 		 				          guifg=#ffecb3 	guibg=NONE 		  gui=NONE
hi Operator 	 				        guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi Statement 	 				        guifg=#69f0ae 	guibg=NONE 		  gui=NONE
" PreProc {{{3
hi Define 		 				        guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi PreProc 		 				        guifg=#69f0ae 	guibg=NONE 		  gui=NONE
" Type {{{3
hi Comment 		 				        guifg=#7986cb 	guibg=NONE 		  gui=NONE
hi StorageClass  				      guifg=#ff80ab 	guibg=NONE 		  gui=italic
hi Type 		 				          guifg=#ff80ab 	guibg=NONE 		  gui=NONE
" Special {{{3
hi Special 		 				        guifg=#f5f5f5 	guibg=NONE 		  gui=NONE
hi SpecialKey 	 				      guifg=#616161 	guibg=NONE 		  gui=NONE
hi Tag 			 				          guifg=#ff80ab 	guibg=NONE 		  gui=NONE
" Spell {{{3
hi SpellBad                   guifg=#f5f5f5   guibg=#ff5252   gui=undercurl
hi SpellCap                   guifg=#212121   guibg=#ffd740   gui=undercurl
hi SpellLocal                 guifg=#212121   guibg=#ffecb3   gui=undercurl
hi SpellRare                  guifg=#ff5252   guibg=#f5f5f5   gui=undercurl
" Other {{{3
hi Directory 	 				        guifg=#80d8ff 	guibg=NONE 		  gui=NONE
hi MatchParen 	 				      guifg=#69f0ae 	guibg=NONE 		  gui=underline
hi NonText 		 				        guifg=#616161 	guibg=NONE 		  gui=NONE
hi SignColumn 	 				      guifg=#7986cb 	guibg=#424242 	gui=NONE
hi Title 		 				          guifg=#f5f5f5 	guibg=NONE 		  gui=bold
hi Todo 		 				          guifg=#18ffff 	guibg=NONE 		  gui=inverse,bold
" }}}
" }}}
" Languages {{{2
" CSS {{{3
hi cssBraces 					        guifg=NONE 		  guibg=NONE 		  gui=NONE
hi cssClassName 				      guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi cssColor 					        guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi cssCommonAttr 				      guifg=#7e57c2 	guibg=NONE 		  gui=NONE
hi cssFunctionName 				    guifg=#ff80ab 	guibg=NONE 		  gui=NONE
hi cssPseudoClassId 			    guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi cssValueLength 				    guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi cssURL 						        guifg=#ffd740 	guibg=NONE 		  gui=italic
" HTML {{{3
hi htmlArg 						        guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi htmlEndTag 					      guifg=NONE 		  guibg=NONE 		  gui=NONE
hi htmlSpecialChar 				    guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi htmlTag 						        guifg=NONE 		  guibg=NONE 		  gui=NONE
hi htmlTagName 					      guifg=#69f0ae 	guibg=NONE 		  gui=NONE
"Java {{{3
hi javaScriptBraces 			    guifg=NONE 		  guibg=NONE 		  gui=NONE
hi javaScriptFunction 			  guifg=#ff80ab 	guibg=NONE 		  gui=italic
hi javaScriptRailsFunction 		guifg=#ff80ab 	guibg=NONE 		  gui=NONE
" Markdown {{{3
hi markdownH1 					      guifg=#18ffff 	guibg=NONE 		  gui=bold
hi markdownH2 					      guifg=#18ffff 	guibg=NONE 		  gui=bold
hi markdownH3 					      guifg=#69f0ae 	guibg=NONE 		  gui=bold
hi markdownH4 					      guifg=#69f0ae 	guibg=NONE 		  gui=bold
hi markdownH5 					      guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi markdownH6 					      guifg=#69f0ae 	guibg=NONE 		  gui=NONE
hi markdownCode 				      guifg=#ffecb3 	guibg=NONE 		  gui=NONE
hi markdownCodeBlock 			    guifg=#ffecb3 	guibg=NONE 		  gui=NONE
hi markdownCodeDelimiter      guifg=#ffecb3 	guibg=NONE 		  gui=NONE
hi markdownBlockquote 			  guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi markdownHeadingRule 			  guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi markdownHeadingDelimiter 	guifg=#ff80ab 	guibg=NONE 		  gui=bold
hi markdownListMarker 			  guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi markdownOrderedListMarker 	guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi markdownRule 				      guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
hi markdownLinkDelimiter 		  guifg=#ffd740 	guibg=NONE 		  gui=italic
hi markdownLinkTextDelimiter 	guifg=#ffd740 	guibg=NONE 		  gui=italic
hi markdownUrl 					      guifg=#18ffff 	guibg=NONE 		  gui=NONE
hi markdownUrlDelimiter 	    guifg=#ffd740 	guibg=NONE 		  gui=italic
hi markdownUrlTitleDelimiter 	guifg=#c8e6c9 	guibg=NONE 		  gui=NONE
" Pandoc {{{3
hi pandocAtxHeader            guifg=#18ffff   guibg=NONE 		  gui=NONE
hi pandocAtxStart             guifg=#80d8ff   guibg=NONE 		  gui=NONE
hi pandocBeginQuote           guifg=#ffd740   guibg=NONE 		  gui=NONE
hi pandocBlockQuote           guifg=#ffecb3   guibg=NONE 		  gui=NONE
hi pandocCiteKey              guifg=#ff80ab   guibg=NONE 		  gui=NONE
hi pandocEllipses             guifg=#ffd740   guibg=NONE 		  gui=NONE
hi pandocEndQuote             guifg=#ffd740   guibg=NONE 		  gui=NONE
hi pandocFootnoteBlock        guifg=#ffecb3   guibg=NONE 		  gui=NONE
hi pandocOperator             guifg=#69f0ae   guibg=NONE 		  gui=NONE
" TeX {{{3
hi texBeginEndName            guifg=#ffecb3   guibg=NONE 		  gui=NONE
hi texDocType					        guifg=#80d8ff 	guibg=NONE 		  gui=NONE
hi texDocTypeArgs   		      guifg=#69f0ae   guibg=NONE 		  gui=NONE
hi texTypeSize                guifg=#ff80ab   guibg=NONE 		  gui=NONE
" Vim Script {{{3
hi vimGroup 					        guifg=#ff80ab 	guibg=NONE 		  gui=NONE
hi vimGroupName 				      guifg=#ff80ab 	guibg=NONE 		  gui=NONE
hi vimHiCtermFgBg 				    guifg=NONE 		  guibg=NONE 		  gui=NONE
hi vimHiGuiFgBg 				      guifg=NONE 		  guibg=NONE 		  gui=NONE
hi vimOption 					        guifg=#ff80ab 	guibg=NONE 		  gui=NONE
" YAML {{{3
hi yamlAlias 					        guifg=NONE 		  guibg=NONE 		  gui=NONE
hi yamlAnchor 					      guifg=NONE 		  guibg=NONE 		  gui=NONE
hi yamlDocumentHeader 			  guifg=#ffd740 	guibg=NONE 		  gui=NONE
hi yamlInteger					      guifg=#69f0ae   guibg=NONE 		  gui=NONE
hi yamlKey 						        guifg=#80d8ff 	guibg=NONE 		  gui=NONE
" }}}
" }}}
" }}}

" Terminal Colors {{{1
" General {{{2
" Modes {{{3
hi Normal 		 				        ctermfg=254   ctermbg=NONE  cterm=NONE
hi Visual 		 				        ctermfg=237  	ctermbg=214 	cterm=NONE
" Completion {{{3
hi Pmenu 		 				          ctermfg=250  	ctermbg=235  	cterm=NONE
hi PmenuSel 	 				        ctermfg=254  	ctermbg=237 	cterm=NONE
" Cursor {{{3
hi ColorColumn 	 				      ctermfg=NONE 	ctermbg=239 	cterm=NONE
hi Cursor 		 				        ctermfg=234		ctermbg=NONE	cterm=NONE
hi CursorColumn 				      ctermfg=NONE  ctermbg=238 	cterm=NONE
hi CursorLine 	 				      ctermfg=NONE  ctermbg=237 	cterm=NONE
hi CursorLineNr	 				      ctermfg=222 	ctermbg=237 	cterm=NONE
hi LineNr 		 				        ctermfg=38 		ctermbg=234  	cterm=NONE
" Diff {{{3
hi DiffAdd 		 				        ctermfg=234 	ctermbg=211   cterm=bold
hi DiffChange 	 				      ctermfg=NONE	ctermbg=237		cterm=NONE
hi DiffDelete 	 				      ctermfg=203		ctermbg=NONE 	cterm=NONE
hi DiffText 	 				        ctermfg=237 	ctermbg=221		cterm=bold
" Fold {{{3
hi FoldColmun 	 				      ctermfg=75  	ctermbg=238 	cterm=NONE
hi Folded 		 				        ctermfg=38 		ctermbg=234 	cterm=NONE
" Messages {{{3
hi ErrorMsg 	 				        ctermfg=254 	ctermbg=197 	cterm=NONE
hi WarningMsg 	 				      ctermfg=254 	ctermbg=202 	cterm=NONE
" Search {{{3
hi IncSearch 	 				        ctermfg=234		ctermbg=214 	cterm=none
hi Search 		 				        ctermfg=234		ctermbg=114		cterm=none
" Split {{{3
hi StatusLine 		 			      ctermfg=254 	ctermbg=237 	cterm=bold
hi StatusLineNC  				      ctermfg=254 	ctermbg=237 	cterm=NONE
hi VertSplit 	 				        ctermfg=237 	ctermbg=237 	cterm=bold
" Constant {{{3
hi Boolean 		 				        ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi Character 	 				        ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi Constant 	 				        ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi Float 		 				          ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi Number 		 				        ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi String 		 				        ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi Underlined 	 				      ctermfg=NONE 	ctermbg=NONE 	cterm=underline
" Indentifier {{{3
hi Function 	 				        ctermfg=114		ctermbg=NONE 	cterm=NONE
hi Identifier 	 				      ctermfg=211 	ctermbg=NONE 	cterm=NONE
" Statement {{{3
hi Conditional 	 				      ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi Keyword 		 				        ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi Label 		 				          ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi Operator 	 				        ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi Statement 	 				        ctermfg=75  	ctermbg=NONE 	cterm=NONE
" PreProc {{{3
hi Define 		 				        ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi PreProc 		 				        ctermfg=75  	ctermbg=NONE 	cterm=NONE
" Type {{{3
hi Comment 		 				        ctermfg=109		ctermbg=NONE 	cterm=italic
hi StorageClass  				      ctermfg=211 	ctermbg=NONE 	cterm=NONE
hi Type 		 				          ctermfg=211 	ctermbg=NONE 	cterm=NONE
" Special {{{3
hi Special 		 				        ctermfg=254 	ctermbg=NONE 	cterm=NONE
hi SpecialKey 	 				      ctermfg=240 	ctermbg=237 	cterm=NONE
hi Tag 			 				          ctermfg=211 	ctermbg=NONE 	cterm=NONE
" Spell {{{3
hi SpellBad                   ctermfg=254   ctermbg=203   cterm=undercurl
hi SpellCap                   ctermfg=234   ctermbg=221   cterm=undercurl
hi SpellLocal                 ctermfg=234   ctermbg=222   cterm=undercurl
hi SpellRare                  ctermfg=203   ctermbg=254   cterm=undercurl
" Other {{{3
hi Directory 	 				        ctermfg=38  	ctermbg=NONE 	cterm=NONE
hi MatchParen 	 				      ctermfg=75  	ctermbg=NONE 	cterm=underline
hi NonText 		 				        ctermfg=240 	ctermbg=NONE 	cterm=NONE
hi SignColumn 	 				      ctermfg=109 	ctermbg=237 	cterm=NONE
hi Title 		 				          ctermfg=254 	ctermbg=NONE 	cterm=bold
hi Todo 		 				          ctermfg=80 		ctermbg=NONE 	cterm=inverse,bold
" }}}
" }}}
" Languages {{{2
" CSS {{{3
hi cssBraces 					        ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi cssClassName 				      ctermfg=114		ctermbg=NONE 	cterm=NONE
hi cssColor 					        ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi cssCommonAttr 				      ctermfg=204		ctermbg=NONE 	cterm=NONE
hi cssFunctionName 				    ctermfg=211 	ctermbg=NONE 	cterm=NONE
hi cssPseudoClassId 			    ctermfg=114		ctermbg=NONE 	cterm=NONE
hi cssValueLength 				    ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi cssURL 						        ctermfg=222 	ctermbg=NONE 	cterm=NONE
" HTML {{{3
hi htmlArg 						        ctermfg=114  	ctermbg=NONE 	cterm=NONE
hi htmlEndTag 					      ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi htmlSpecialChar 				    ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi htmlTag 						        ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi htmlTagName 					      ctermfg=75   	ctermbg=NONE 	cterm=NONE
"Java {{{3
hi javaScriptBraces 			    ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi javaScriptFunction 			  ctermfg=211 	ctermbg=NONE 	cterm=NONE
hi javaScriptRailsFunction 		ctermfg=211 	ctermbg=NONE 	cterm=NONE
" Markdown {{{3
hi markdownH1 					      ctermfg=80  	ctermbg=NONE 	cterm=bold
hi markdownH2 					      ctermfg=80  	ctermbg=NONE 	cterm=bold
hi markdownH3 					      ctermfg=75  	ctermbg=NONE 	cterm=bold
hi markdownH4 					      ctermfg=75  	ctermbg=NONE 	cterm=bold
hi markdownH5 					      ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi markdownH6 					      ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi markdownCode 				      ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi markdownCodeBlock 			    ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi markdownCodeDelimiter 		  ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi markdownBlockquote 			  ctermfg=114		ctermbg=NONE 	cterm=NONE
hi markdownHeadingRule 			  ctermfg=114		ctermbg=NONE 	cterm=NONE
hi markdownHeadingDelimiter 	ctermfg=211 	ctermbg=NONE 	cterm=bold
hi markdownListMarker 			  ctermfg=114		ctermbg=NONE 	cterm=NONE
hi markdownOrderedListMarker 	ctermfg=114		ctermbg=NONE 	cterm=NONE
hi markdownRule 				      ctermfg=114		ctermbg=NONE 	cterm=NONE
hi markdownLinkDelimiter 		  ctermfg=214 	ctermbg=NONE 	cterm=NONE
hi markdownLinkTextDelimiter  ctermfg=214 	ctermbg=NONE 	cterm=NONE
hi markdownUrl 					      ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi markdownUrlDelimiter 	    ctermfg=214 	ctermbg=NONE 	cterm=NONE
hi markdownUrlTitleDelimiter 	ctermfg=114		ctermbg=NONE 	cterm=NONE
" Pandoc {{{3
hi pandocAtxHeader            ctermfg=80    ctermbg=NONE 	cterm=NONE
hi pandocAtxStart             ctermfg=38    ctermbg=NONE 	cterm=NONE
hi pandocBeginQuote           ctermfg=214   ctermbg=NONE 	cterm=NONE
hi pandocBlockQuote           ctermfg=222   ctermbg=NONE 	cterm=NONE
hi pandocCiteKey              ctermfg=211   ctermbg=NONE 	cterm=NONE
hi pandocEllipses             ctermfg=214   ctermbg=NONE 	cterm=NONE
hi pandocEndQuote             ctermfg=214   ctermbg=NONE 	cterm=NONE
hi pandocFootnoteBlock        ctermfg=222   ctermbg=NONE 	cterm=NONE
hi pandocOperator             ctermfg=75    ctermbg=NONE 	cterm=NONE
" TeX {{{3
hi texBeginEndName            ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi texDocType					        ctermfg=80  	ctermbg=NONE 	cterm=NONE
hi texDocTypeArgs   		      ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi texTypeSize                ctermfg=211		ctermbg=NONE 	cterm=NONE
" Vim Script {{{3
hi vimGroup 					        ctermfg=211		ctermbg=NONE 	cterm=NONE
hi vimGroupName 				      ctermfg=211		ctermbg=NONE 	cterm=NONE
hi vimHiCtermFgBg 				    ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi vimHiGuiFgBg 				      ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi vimOption 					        ctermfg=211		ctermbg=NONE 	cterm=NONE
" YAML {{{3
hi yamlAlias 					        ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi yamlAnchor 					      ctermfg=NONE 	ctermbg=NONE 	cterm=NONE
hi yamlDocumentHeader 			  ctermfg=222 	ctermbg=NONE 	cterm=NONE
hi yamlInteger  			        ctermfg=75  	ctermbg=NONE 	cterm=NONE
hi yamlKey 						        ctermfg=80  	ctermbg=NONE 	cterm=NONE
" }}}
" }}}
" }}}

" Fold Method {{{1
" vim:foldmethod=marker
